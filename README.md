# slue-stream

Stream tools, used to get a transform or readable stream。

## transformObj
```javascript
const slueStream = require('slue-stream');
let sluePlugin = slueStream.transformObj(function(file, evn, cb) {
    cb(null, file);
});
```

## combine
combine streams
```javascript
    let stream = slueStream.combine([stream1, stream2])
```

## readable
```javascript
    let stream = slueStream.readable()
```